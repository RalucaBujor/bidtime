//
//  AddProductViewController.swift
//  BidTime
//
//  Created by Raluca Bujor on 23/10/2018.
//  Copyright © 2018 Raluca Bujor. All rights reserved.
//

import UIKit

class AddProductViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var addPhotoOutlet: UIButton!
    @IBOutlet weak var nameOfProduct: UITextField!
    @IBOutlet weak var descriptionOfProduct: UITextView!
    @IBOutlet weak var endTime: UITextField!
    @IBOutlet weak var lowestBid: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: Variables
    var productsInDatabase: [String:String] = [:]
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.isHidden = true
        setPlaceHolderToTextView()
        addTapGesture()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getProductsFromDatabase()
        addObserversForKeyboard()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObserversForKeyboard()
    }
    
    //MARK: Helpers
    func setPlaceHolderToTextView() {
        descriptionOfProduct.text = "Description"
        descriptionOfProduct.textColor = UIColor.lightGray
    }
    
    func addObserversForKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func removeObserversForKeyboard() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    func addTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        nameOfProduct.resignFirstResponder()
        descriptionOfProduct.resignFirstResponder()
        endTime.resignFirstResponder()
        lowestBid.resignFirstResponder()
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        if Date().toString(dateFormat: "HH:mm:ss") > (sender.date).toString(dateFormat: "HH:mm:ss") {
            self.addAlert(title: "Alert", message: "Please enter a valid time, after current hour!")
        } else {
            endTime.text = (sender.date).toString(dateFormat: "HH:mm:ss")
        }
    }
    
    func validateField() -> Bool {
        if addPhotoOutlet.image(for: .normal) == nil || nameOfProduct == nil || endTime == nil || lowestBid == nil || nameOfProduct.text! == "" || endTime.text! == "" || lowestBid.text! == "" {
            self.addAlert(title: "Alert", message: "The photo, the name of the product, the end time and the lowest bid are mandatory!")
            return false
        } else if Int(lowestBid.text!) == nil {
            self.addAlert(title: "Alert", message: "Please add a valid number for the price of the product!")
            return false
        }else if nameOfProduct.text?.contains(".") == true || nameOfProduct.text?.contains("#") == true || nameOfProduct.text?.contains("$") == true || nameOfProduct.text?.contains("[.]") == true || nameOfProduct.text?.contains("]") == true {
            self.addAlert(title: "Alert", message: "The name of the product must be a non-empty string and not contain '.' '#' '$' '[' or ']'")
            return false
        }
        return true
    }
    
    //MARK: Actions
    @IBAction func addPhotoButton(_ sender: UIButton) {
        let dialogMessage = UIAlertController(title: "", message: "Choose an action :", preferredStyle: .actionSheet)
        let photoLibrary = UIAlertAction(title: "Photo Library", style: .default, handler: { (action) -> Void in
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        })
        
        let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { (action) -> Void in
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            
            if !UIImagePickerController.isSourceTypeAvailable(.camera){
                self.addAlert(title: "No camera", message: "You device has no camera available!")
            }
            else{
                imagePicker.sourceType = UIImagePickerController.SourceType.camera
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        dialogMessage.addAction(photoLibrary)
        dialogMessage.addAction(takePhoto)
        dialogMessage.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    @IBAction func addProductButton(_ sender: UIButton) {
        endTime.resignFirstResponder()
        if validateField() == true && productsInDatabase[nameOfProduct.text!] == nameOfProduct.text! {
            self.addAlert(title: "Alert", message: "A product with the same name already exists! Please enter a new name for your product !")
        } else if validateField() == true {
            if descriptionOfProduct == nil {
                descriptionOfProduct.text = ""
            }
            let image = addPhotoOutlet.image(for: .normal)
            if lowestBid != nil || endTime != nil {
                activityIndicator.isHidden = false
                activityIndicator.startAnimating()
                addProductToDatabase(nameOfProduct: nameOfProduct.text!, descriptionOfProduct: descriptionOfProduct.text!, endTime: endTime.text!, lowestBid: lowestBid.text!, photoForProduct: image!, endTimeDay: Date().toString(dateFormat: "yyyy:MM:dd"))
            }
        }
    }
    
    @IBAction func endTimePicker(_ sender: UITextField) {
        let datePickerView  : UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePicker.Mode.time
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker), for: UIControl.Event.valueChanged)
    }
}
