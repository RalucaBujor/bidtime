//
//  SettingsViewController.swift
//  BidTime
//
//  Created by Raluca Bujor on 24/10/2018.
//  Copyright © 2018 Raluca Bujor. All rights reserved.
//

import UIKit
import Firebase

class SettingsViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var profilePhotoOutlet: UIButton!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: Variables
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapGesture()
        putInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObserversForKeyboard()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObserversForKeyboard()
    }
    
    //MARK: Helpers
    func addTapGesture() {
        activityIndicator.isHidden = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        firstNameTextField.resignFirstResponder()
        lastNameTextField.resignFirstResponder()
    }
    
    func addObserversForKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func removeObserversForKeyboard() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    //MARk: Action
    @IBAction func profilePhotoAction(_ sender: UIButton) {
        let dialogMessage = UIAlertController(title: "", message: "Choose an action :", preferredStyle: .actionSheet)
        // Create OK button with action handler
        let photoLibrary = UIAlertAction(title: "Photo Library", style: .default, handler: { (action) -> Void in
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        })
        
        let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { (action) -> Void in
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            
            if !UIImagePickerController.isSourceTypeAvailable(.camera){
                self.addAlert(title: "No camera", message: "You device has no camera available!")
            }
            else{
                imagePicker.sourceType = UIImagePickerController.SourceType.camera
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        dialogMessage.addAction(photoLibrary)
        dialogMessage.addAction(takePhoto)
        dialogMessage.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    @IBAction func saveInfoButton(_ sender: UIButton) {
        let userID = Auth.auth().currentUser?.uid
        
        if userID != nil {
            if profilePhotoOutlet.image(for: .normal) == nil && firstNameTextField != nil && lastNameTextField != nil && firstNameTextField.text != "" && lastNameTextField.text != "" {
                updateNameFromSaveButton(userID: userID!, firstName: self.firstNameTextField.text!, lastName: self.lastNameTextField.text!)
            }
            if firstNameTextField != nil && lastNameTextField != nil && firstNameTextField.text != "" && lastNameTextField.text != "" && profilePhotoOutlet.image(for: .normal) != nil {
                activityIndicator.isHidden = false
                activityIndicator.startAnimating()
                updateValuesFromSaveButton(userID: userID!, firstName: self.firstNameTextField.text!, lastName: self.lastNameTextField.text!, profilePhoto: profilePhotoOutlet.image(for: .normal)!)
            } else {
                self.addAlert(title: "Alert", message: "Please enter name in order to save information!")
            }
            
        }
    }
    
    @IBAction func logOutButton(_ sender: UIButton) {
        let dialogMessage = UIAlertController(title: "", message: "Are you sure you want to log out?", preferredStyle: .actionSheet)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            do {
                try Auth.auth().signOut()
            }catch {
                print("Nu s-a autentificat")
            }
            
            guard let myVC = self.storyboard?.instantiateViewController(withIdentifier: "login") else { return }
            let navController = UINavigationController(rootViewController: myVC)
            
            self.navigationController?.present(navController, animated: true, completion: nil)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            print("Cancel button tapped")
        }
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
}
