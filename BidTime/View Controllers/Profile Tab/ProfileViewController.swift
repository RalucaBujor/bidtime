//
//  ProfileViewController.swift
//  BidTime
//
//  Created by Raluca Bujor on 24/10/2018.
//  Copyright © 2018 Raluca Bujor. All rights reserved.
//

import UIKit
import Firebase
class ProfileViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var profileTableView: UITableView!
    
    //MARK: Variables
    var wonProducts: [Product] = []
    var pendingProducts: [Product] = []
    var addedProducts: [Product] = []
    var refProducts: DatabaseReference!
    var typeOfTable: Int = 1
    var indexForCellWithProduct: Int = 0
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ProductsViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.blue
        
        return refreshControl
    }()
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileTableView.tableFooterView = UIView()
        self.fetchProductsFromFirebase()
        self.profileTableView.addSubview(self.refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fetchProductsFromFirebase()
    }
    
    //MARK: Helpers
    func validateTime(endTime: String) -> Bool{
        let requestedComponent: Set<Calendar.Component> = [.hour,.minute,.second]
        let userCalendar = Calendar.current
        
        let outputFormat = Date().toString(dateFormat: "HH:mm:ss")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let startTime = dateFormatter.date(from: outputFormat)
        let endTimes = dateFormatter.date(from: endTime)
        let timeDifference = userCalendar.dateComponents(requestedComponent, from: startTime!, to: endTimes!)
        if timeDifference.hour! < 0 || timeDifference.minute! < 0 || timeDifference.second! < 0 {
            return false
        }
        return true
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        fetchProductsFromFirebase()
        self.profileTableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    //MARK: Actions
    @IBAction func wonAuctions(_ sender: UIButton) {
        typeOfTable = 1
        self.profileTableView.reloadData()
    }
    
    @IBAction func pendingAuctions(_ sender: UIButton) {
        typeOfTable = 2
        self.profileTableView.reloadData()
    }
    
    @IBAction func addedAuctions(_ sender: UIButton) {
        typeOfTable = 3
        self.profileTableView.reloadData()
    }
    
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailedScreenForProduct", let destinationVC = segue.destination as? DetailedScreenForProductViewController {
            var productsListForDetail: [Product] = []
            if self.typeOfTable == 1 {
                productsListForDetail = self.wonProducts
            } else if self.typeOfTable == 2 {
                productsListForDetail = self.pendingProducts
            } else {
                productsListForDetail = self.addedProducts
            }
            
            destinationVC.nameOfProduct = productsListForDetail[indexForCellWithProduct].nameOfProduct
            destinationVC.descriptionOfProduct = productsListForDetail[indexForCellWithProduct].descriptionOfProduct
            destinationVC.endTime = productsListForDetail[indexForCellWithProduct].endTime
            destinationVC.lowestBid = productsListForDetail[indexForCellWithProduct].maximPaid
            destinationVC.photoForProduct = productsListForDetail[indexForCellWithProduct].photoForProduct
            destinationVC.userWhoCreatedAcution = productsListForDetail[indexForCellWithProduct].userWhoCreatedAcution
            destinationVC.endTimeDay = productsListForDetail[indexForCellWithProduct].endTimeDay
        }
    }
}
