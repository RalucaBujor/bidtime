//
//  ProductsViewController.swift
//  BidTime
//
//  Created by Raluca Bujor on 23/10/2018.
//  Copyright © 2018 Raluca Bujor. All rights reserved.
//

import UIKit
import Firebase

class ProductsViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: Variables
    var refProducts: DatabaseReference!
    var productsList = [Product]()
    var indexForCellWithProduct: Int = 0
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ProductsViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.blue
        
        return refreshControl
    }()
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        addTapForKeyboard()
        fetchDataFromFirebareForProducts()
        self.tableView.addSubview(self.refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchDataFromFirebareForProducts()
        tableView.reloadData()
    }
    
    //MARK: Helpers
    func addTapForKeyboard() {
        self.tableView.tableFooterView = UIView()
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.singleTap(sender:)))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(singleTapGestureRecognizer)
    }
    
    @objc func singleTap(sender: UITapGestureRecognizer) {
        self.searchBar.resignFirstResponder()
    }
    
    func validateTime(endTime: String) -> Bool{
        let requestedComponent: Set<Calendar.Component> = [.hour,.minute,.second]
        let userCalendar = Calendar.current
        
        let outputFormat = Date().toString(dateFormat: "HH:mm:ss")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let startTime = dateFormatter.date(from: outputFormat)
        let endTimes = dateFormatter.date(from: endTime)
        let timeDifference = userCalendar.dateComponents(requestedComponent, from: startTime!, to: endTimes!)
        if timeDifference.hour! < 0 || timeDifference.minute! < 0 || timeDifference.second! < 0 {
            return false
        }
        return true
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        fetchDataFromFirebareForProducts()
        self.tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    //MARK: Actions
    
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailedScreenForProduct", let destinationVC = segue.destination as? DetailedScreenForProductViewController {
            destinationVC.nameOfProduct = productsList[indexForCellWithProduct].nameOfProduct
            destinationVC.descriptionOfProduct = productsList[indexForCellWithProduct].descriptionOfProduct
            destinationVC.endTime = productsList[indexForCellWithProduct].endTime
            destinationVC.lowestBid = productsList[indexForCellWithProduct].maximPaid
            destinationVC.photoForProduct = productsList[indexForCellWithProduct].photoForProduct
            destinationVC.userWhoCreatedAcution = productsList[indexForCellWithProduct].userWhoCreatedAcution
            destinationVC.endTimeDay = productsList[indexForCellWithProduct].endTimeDay
        }
    }
    
}
