//
//  DetailedScreenForProductViewController.swift
//  BidTime
//
//  Created by Raluca Bujor on 23/10/2018.
//  Copyright © 2018 Raluca Bujor. All rights reserved.
//

import UIKit
import Firebase

class DetailedScreenForProductViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var descriptionOutlet: UILabel!
    @IBOutlet weak var bid: UILabel!
    @IBOutlet weak var timeLeft: UILabel!
    
    //MARK: Variables
    var nameOfProduct: String = ""
    var descriptionOfProduct: String = ""
    var endTime: String = ""
    var lowestBid: String = ""
    var photoForProduct: String = ""
    var userWhoCreatedAcution: String = ""
    var endTimeDay: String = ""
    let requestedComponent: Set<Calendar.Component> = [.hour,.minute,.second]
    let userCalendar = Calendar.current
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = nameOfProduct
        setValuesForLabels()
    }
    
    //MARK: Helpers
    func setValuesForLabels() {
        photo.cacheImage(urlString: photoForProduct)
        descriptionOutlet.text = descriptionOfProduct
        bid.text = "Current bid: $" + lowestBid
        if endTimeDay == Date().toString(dateFormat: "yyyy:MM:dd") {
            Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(getTime), userInfo: nil, repeats: true)
        } else {
            timeLeft.text = "Ended \(endTimeDay) at \(endTime)"
        }
    }
    
    @objc func getTime() {
        let outputFormat = Date().toString(dateFormat: "HH:mm:ss")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let startTime = dateFormatter.date(from: outputFormat)
        let endTimes = dateFormatter.date(from: endTime)
        let timeDifference = userCalendar.dateComponents(requestedComponent, from: startTime!, to: endTimes!)
        if timeDifference.hour! < 0 || timeDifference.minute! < 0 || timeDifference.second! < 0 {
            timeLeft.text = "Ended at \(endTime)"
        } else {
            timeLeft.text = "Time left " + String(timeDifference.hour!) + ":" + String(timeDifference.minute!) + ":" + String(timeDifference.second!)
        }
    }
    
    //MARK: Actions
    @IBAction func bidNowButton(_ sender: Any) {
        getCurrentBid(nameOfProduct: nameOfProduct)
        if userWhoCreatedAcution == Auth.auth().currentUser!.uid {
            self.addAlert(title: "Alert", message: "You can't bid on a product that you have created!")
        } else if timeLeft.text!.prefix(5) == "Ended" {
            self.addAlert(title: "Alert", message: "You can't bid on a product that ended!")
        }else {
            let alert = UIAlertController(title: "Bid Now", message: "Enter the amount you would like to bid!", preferredStyle: .alert)
            alert.addTextField { (textField) in
                textField.keyboardType = .numberPad
            }
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                let textField = alert!.textFields![0] // Force unwrapping because we know it exists.
                if let valueToBid = Int(textField.text!), let lowestBid = Int(self.lowestBid) {
                    if valueToBid < lowestBid {
                        let alert = UIAlertController(title: "Alert", message: "You need to bid with a higher value than the current one!", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        self.lowestBid = String(valueToBid)
                        self.bid.text = "Current bid: $" + self.lowestBid
                        self.setNewValueForBidToDatabase(nameOfProduct: self.nameOfProduct, currentbid:  String(valueToBid))
                    }
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}
