//
//  ProfileTableViewCell.swift
//  BidTime
//
//  Created by Raluca Bujor on 24/10/2018.
//  Copyright © 2018 Raluca Bujor. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var photoForProduct: UIImageView!
    @IBOutlet weak var nameForTheProduct: UILabel!
    @IBOutlet weak var currentBidForTheProduct: UILabel!
    @IBOutlet weak var timeLeftForTheProduct: UILabel!
    
    //MARK: Variables
    var endTimeForProduct: String = ""
    let requestedComponent: Set<Calendar.Component> = [.hour,.minute,.second]
    let userCalendar = Calendar.current
    
    //MARK: Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK: Helpers
    func addValuesToCell(photoUrlForProduct: String,nameOfProductValue: String, currentBidValue: String, endTimeValue: String, endTimeDay: String ) {
        photoForProduct.cacheImage(urlString: photoUrlForProduct)
        nameForTheProduct.text = nameOfProductValue
        currentBidForTheProduct.text = "US $" + currentBidValue
        if endTimeDay == Date().toString(dateFormat: "yyyy:MM:dd"){
            endTimeForProduct = endTimeValue
            Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(getTime), userInfo: nil, repeats: true)
        } else {
            timeLeftForTheProduct.text = "Ended: \(endTimeDay), at \(endTimeValue)"
        }
    }
    
    @objc func getTime() {
        let outputFormat = Date().toString(dateFormat: "HH:mm:ss")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let startTime = dateFormatter.date(from: outputFormat)
        let endTimes = dateFormatter.date(from: endTimeForProduct)
        let timeDifference = userCalendar.dateComponents(requestedComponent, from: startTime!, to: endTimes!)
        if timeDifference.hour! < 0 || timeDifference.minute! < 0 || timeDifference.second! < 0 {
            timeLeftForTheProduct.text = "Ended at: \(endTimeForProduct)"
        } else {
            timeLeftForTheProduct.text = "Time left: " + String(timeDifference.hour!) + ":" + String(timeDifference.minute!) + ":" + String(timeDifference.second!)
        }
    }

}
