//
//  ProductsTableViewCell.swift
//  BidTime
//
//  Created by Raluca Bujor on 23/10/2018.
//  Copyright © 2018 Raluca Bujor. All rights reserved.
//

import UIKit

class ProductsTableViewCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var photoProduct: UIImageView!
    @IBOutlet weak var nameOfProduct: UILabel!
    @IBOutlet weak var currentBid: UILabel!
    @IBOutlet weak var timeLeft: UILabel!
    
    //MARK: Variables
    var endTime: String = ""
    let requestedComponent: Set<Calendar.Component> = [.hour,.minute,.second]
    let userCalendar = Calendar.current
    
    //MARK: Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Helpers
    func addValuesToCell(photoUrlForProduct: String,nameOfProductValue: String, currentBidValue: String, endTimeValue: String ) {
        photoProduct.cacheImage(urlString: photoUrlForProduct)
        nameOfProduct.text = nameOfProductValue
        currentBid.text = "US $" + currentBidValue
        endTime = endTimeValue
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(getTime), userInfo: nil, repeats: true)
    }
    
    @objc func getTime() {
        let outputFormat = Date().toString(dateFormat: "HH:mm:ss")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let startTime = dateFormatter.date(from: outputFormat)
        let endTimes = dateFormatter.date(from: endTime)
        let timeDifference = userCalendar.dateComponents(requestedComponent, from: startTime!, to: endTimes!)
        if timeDifference.hour! < 0 || timeDifference.minute! < 0 || timeDifference.second! < 0 {
            timeLeft.text = "Ended at: \(endTime)"
        } else {
            timeLeft.text = "Time left: " + String(timeDifference.hour!) + ":" + String(timeDifference.minute!) + ":" + String(timeDifference.second!)
        }
    }
    
    //MARK: Actions

}
