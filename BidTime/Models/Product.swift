//
//  Product.swift
//  BidTime
//
//  Created by Raluca Bujor on 23/10/2018.
//  Copyright © 2018 Raluca Bujor. All rights reserved.
//

import Foundation
import UIKit

class Product {
    let nameOfProduct: String
    let descriptionOfProduct: String
    let endTime: String
    let photoForProduct: String
    var userWhoCreatedAcution: String = ""
    var lowestBid: String = ""
    var userWhoPaidTheMost: String = ""
    var maximPaid: String
    let endTimeDay: String
        
    init(nameOfProduct: String, descriptionOfProduct: String, endTime: String, maximPaid: String, photoForProduct: String, endTimeDay: String, userWhoPaidTheMost: String, userWhoCreatedAcution: String, lowestBid: String) {
        self.nameOfProduct = nameOfProduct
        self.descriptionOfProduct = descriptionOfProduct
        self.endTime = endTime
        self.maximPaid = maximPaid
        self.photoForProduct = photoForProduct
        self.endTimeDay = endTimeDay
        self.userWhoPaidTheMost = userWhoPaidTheMost
        self.userWhoCreatedAcution = userWhoCreatedAcution
        self.lowestBid = lowestBid
    }
}
