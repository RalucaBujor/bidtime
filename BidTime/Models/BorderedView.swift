//
//  BorderedView.swift
//  BidTime
//
//  Created by Raluca Bujor on 23/10/2018.
//  Copyright © 2018 Raluca Bujor. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class BorderedView: UIImageView {
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
}
