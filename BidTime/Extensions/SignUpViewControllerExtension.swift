//
//  SignUpViewControllerExtension.swift
//  BidTime
//
//  Created by Raluca Bujor on 25/10/2018.
//  Copyright © 2018 Raluca Bujor. All rights reserved.
//

import Foundation
import UIKit
import Firebase

extension SignUpViewController {
    func createUser(with email: String, and password: String){
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if user != nil {
                let ref = Database.database().reference();
                let userID = Auth.auth().currentUser?.uid
                ref.child("users").child(userID!).setValue( ["id":userID!, "email":email, "profilePhotoUrl":"profile100", "firstName":" ", "lastName":" ", "numberOfProducts":0])
                
                print("User has Signed UP")
                self.performSegue(withIdentifier: "private", sender: self)
            }
            if error != nil {
                //there is an error
                self.addAlert(title:  "Alert", message: String(error!.localizedDescription))
                print("Something went wrong")
            }
        }
    }
}
