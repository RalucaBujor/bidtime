//
//  SignInViewControllerExtension.swift
//  BidTime
//
//  Created by Raluca Bujor on 25/10/2018.
//  Copyright © 2018 Raluca Bujor. All rights reserved.
//

import Foundation
import UIKit
import Firebase

extension SignInViewController: UITextFieldDelegate {
    
    //MARK: TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            self.viewForEmail.isHidden = true
        }
        if textField.tag == 2 {
            self.viewForPassword.isHidden = true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1 {
           self.viewForEmail.isHidden = false
        }
        if textField.tag == 2 {
            self.viewForPassword.isHidden = false
        }
    }
    
    //MARK: Firebase related
    func checkIfCurrentUserExists() {
        if Auth.auth().currentUser?.uid == nil {
            do {
                try Auth.auth().signOut()
            }catch {
                print("Nu s-a autentificat")
            }
        } else {
            self.performSegue(withIdentifier: "private", sender: self)
        }
    }
    
    func loginUser(with email: String, and password: String){
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if user != nil {
                print("User has Signed IN")
                self.performSegue(withIdentifier: "private", sender: self)
            }
            if error != nil {
                //there is an error
                self.addAlert(title: "Alert", message: String(error!.localizedDescription))
                print("Something went wrong")
            }
        }
    }
    
}
