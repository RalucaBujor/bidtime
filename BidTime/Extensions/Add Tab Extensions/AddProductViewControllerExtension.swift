//
//  AddProductViewControllerExtension.swift
//  BidTime
//
//  Created by Raluca Bujor on 23/10/2018.
//  Copyright © 2018 Raluca Bujor. All rights reserved.
//

import Foundation
import UIKit
import Firebase

extension AddProductViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate{
    
    //MARK: Image Picker Controller Delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var selectedImageFromPicker: UIImage?
        
        guard let _ = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            addPhotoOutlet.setImage(selectedImage, for: .normal)
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("canceled picker")
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: TextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Description"
            textView.textColor = UIColor.lightGray
        }
    }
    
    //MARK: Firebase related
    func addProductToDatabase(nameOfProduct: String, descriptionOfProduct: String, endTime: String, lowestBid: String, photoForProduct: UIImage, endTimeDay: String) {
        let ref = Database.database().reference();
        
        let storageRef = Storage.storage().reference().child(nameOfProduct).child("photoForProduct").child("image.png")
        if let uploadData = photoForProduct.pngData() {
            storageRef.putData(uploadData, metadata: nil, completion: {
                (metadata, error) in
                if error != nil {
                    print(error!)
                    return
                }
                print(metadata!)
                storageRef.downloadURL { (URL, error) -> Void in
                    if (error != nil) {
                    } else {
                        let UrlString = URL!.absoluteString
                        ref.child("products").child(nameOfProduct).setValue( ["nameOfProduct":nameOfProduct, "descriptionOfProduct":descriptionOfProduct, "endTime":endTime, "endTimeDay": endTimeDay, "lowestBid":lowestBid, "photoForProduct": UrlString, "userWhoCreatedAcution": (Auth.auth().currentUser?.uid)!, "userWhoPaidTheMost": "nobody", "maximPaid" : lowestBid])
                        ref.child("users").child((Auth.auth().currentUser?.uid)!).child("products").child(nameOfProduct).setValue( ["nameOfProduct":nameOfProduct, "descriptionOfProduct":descriptionOfProduct, "endTime":endTime, "lowestBid":lowestBid, "photoForProduct": UrlString])
                        self.nameOfProduct.text = ""
                        self.descriptionOfProduct.text = ""
                        self.endTime.text = ""
                        self.lowestBid.text = ""
                        self.addPhotoOutlet.setImage(nil, for: .normal)
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.isHidden = true
                    }
                }
            })
        }
        
    }
    
    func getProductsFromDatabase(){
        let refUsers = Database.database().reference().child("products")
        refUsers.observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            self.productsInDatabase = [:]
            for product in snapshot.children.allObjects as! [DataSnapshot] {
                self.productsInDatabase[product.key] = product.key
            }
        })
    }
}
