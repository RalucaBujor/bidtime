//
//  ProductsViewController.swift
//  BidTime
//
//  Created by Raluca Bujor on 23/10/2018.
//  Copyright © 2018 Raluca Bujor. All rights reserved.
//

import Foundation
import UIKit
import Firebase

extension ProductsViewController: UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    //MARK: TableView Delegates
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProductsTableViewCell
        
        let product: Product
        product = productsList[indexPath.row]
        cell.addValuesToCell(photoUrlForProduct: product.photoForProduct, nameOfProductValue: product.nameOfProduct, currentBidValue: product.maximPaid, endTimeValue: product.endTime)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productsList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexForCellWithProduct = indexPath.row
        performSegue(withIdentifier: "detailedScreenForProduct", sender: self)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete && productsList[indexPath.row].userWhoCreatedAcution == (Auth.auth().currentUser!.uid) && productsList[indexPath.row].maximPaid == productsList[indexPath.row].lowestBid {
            print("Deleted")
            
            removeCommentFromFirebase(product: productsList[indexPath.row].nameOfProduct, user: (Auth.auth().currentUser!.uid))
            self.productsList.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        } else if editingStyle == .delete && productsList[indexPath.row].maximPaid != productsList[indexPath.row].lowestBid{
            self.addAlert(title: "Alert", message: "Cannot delete products that have been already bided")
        } else {
            self.addAlert(title: "Alert", message: "Cannot delete products from other users")
        }
    }
    
    //MARK: Search Bar Delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchText \(searchText)")
        fetchDataFromFirebare(s: searchText)
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchText \(String(describing: searchBar.text!))")
    }
    
    //MARK: Firebase related
    func removeCommentFromFirebase(product: String, user: String) {
        let ref = Database.database().reference()
        ref.child("products").child(product).removeValue()
        ref.child("users").child(user).child("products").child(product).removeValue()
        self.tableView.reloadData()
        
    }
    
    func fetchDataFromFirebareForProducts() {
        
        refProducts = Database.database().reference().child("products")
        refProducts.observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                self.productsList.removeAll()
                for product in snapshot.children.allObjects as! [DataSnapshot] {
                    
                    let userObject = product.value as? [String: AnyObject]
                    let nameOfProduct = userObject?["nameOfProduct"]
                    let descriptionOfProduct = userObject?["descriptionOfProduct"]
                    let endTime  = userObject?["endTime"]
                    let maximPaid  = userObject?["maximPaid"]
                    let photoForProduct = userObject?["photoForProduct"]
                    let endTimeDay = userObject?["endTimeDay"]
                    let userWhoCreatedAcution = userObject?["userWhoCreatedAcution"]
                    let lowestBid = userObject?["lowestBid"]
                    let userWhoPaidTheMost = userObject?["userWhoPaidTheMost"]
                    
                    let product = Product(nameOfProduct: (nameOfProduct as! String?)!, descriptionOfProduct: (descriptionOfProduct as! String?)!, endTime: (endTime as! String?)!, maximPaid: (maximPaid as! String?)!, photoForProduct: (photoForProduct as! String?)!, endTimeDay: (endTimeDay as! String?)!, userWhoPaidTheMost: (userWhoPaidTheMost as! String?)!, userWhoCreatedAcution: (userWhoCreatedAcution as! String?)!, lowestBid: (lowestBid as! String?)!)
                    
                    if product.endTimeDay == Date().toString(dateFormat: "yyyy:MM:dd") && self.validateTime(endTime: (endTime as! String?)!) == true {
                        self.productsList.append(product)
                    }
                    
                }
                self.tableView.reloadData()
            }
        })
    }
    
    func fetchDataFromFirebare(s: String) {
        refProducts = Database.database().reference().child("products")
        refProducts.observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                self.productsList.removeAll()
                for product in snapshot.children.allObjects as! [DataSnapshot] {
                    
                    let userObject = product.value as? [String: AnyObject]
                    let nameOfProduct = userObject?["nameOfProduct"]
                    let descriptionOfProduct = userObject?["descriptionOfProduct"]
                    let endTime  = userObject?["endTime"]
                    let maximPaid  = userObject?["maximPaid"]
                    let lowestBid = userObject?["lowestBid"]
                    let photoForProduct = userObject?["photoForProduct"]
                    let endTimeDay = userObject?["endTimeDay"]
                    let userWhoCreatedAcution = userObject?["userWhoCreatedAcution"]
                    let userWhoPaidTheMost = userObject?["userWhoPaidTheMost"]
                    
                    let product = Product(nameOfProduct: (nameOfProduct as! String?)!, descriptionOfProduct: (descriptionOfProduct as! String?)!, endTime: (endTime as! String?)!, maximPaid: (maximPaid as! String?)!, photoForProduct: (photoForProduct as! String?)!, endTimeDay: (endTimeDay as! String?)!, userWhoPaidTheMost: (userWhoPaidTheMost as! String?)!, userWhoCreatedAcution: (userWhoCreatedAcution as! String?)!, lowestBid: (lowestBid as! String?)!)
                    
                    if s.isEmpty && product.endTimeDay == Date().toString(dateFormat: "yyyy:MM:dd") && self.validateTime(endTime: (endTime as! String?)!) == true {
                        self.productsList.append(product)
                    } else if (product.nameOfProduct.lowercased()).range(of: s.lowercased()) != nil && self.validateTime(endTime: (endTime as! String?)!) == true && product.endTimeDay == Date().toString(dateFormat: "yyyy:MM:dd") {
                        self.productsList.append(product)
                    }
                    
                }
                self.tableView.reloadData()
            }
        })
    }
    
}
