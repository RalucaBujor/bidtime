//
//  DetailedScreenForProductViewControllerExtension.swift
//  BidTime
//
//  Created by Raluca Bujor on 23/10/2018.
//  Copyright © 2018 Raluca Bujor. All rights reserved.
//

import Foundation
import UIKit
import Firebase

extension DetailedScreenForProductViewController {
    
    //MARK: Firebase related
    func setNewValueForBidToDatabase(nameOfProduct: String, currentbid:  String) {
        let ref = Database.database().reference()
        ref.child("products").child(nameOfProduct).child("maximPaid").setValue(currentbid)
        ref.child("products").child(nameOfProduct).child("userWhoPaidTheMost").setValue((Auth.auth().currentUser!.uid))
    }
    
    func getCurrentBid(nameOfProduct: String) {
        var refProducts: DatabaseReference!
        refProducts = Database.database().reference().child("products").child(nameOfProduct).child("maximPaid")
        refProducts.observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            self.bid.text = "Current bid: $" + (snapshot.value! as? String ?? "")
        })
    }
}
