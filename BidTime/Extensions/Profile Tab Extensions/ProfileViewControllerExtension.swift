//
//  ProfileViewControllerExtension.swift
//  BidTime
//
//  Created by Raluca Bujor on 24/10/2018.
//  Copyright © 2018 Raluca Bujor. All rights reserved.
//

import Foundation
import UIKit
import Firebase

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource  {
    
    //MARK: TableView Delegates
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "profileCell", for: indexPath) as! ProfileTableViewCell
        
        let product: Product
        if self.typeOfTable == 1 {
            product = wonProducts[indexPath.row]
        } else if self.typeOfTable == 2 {
            product = pendingProducts[indexPath.row]
        } else {
            product = addedProducts[indexPath.row]
        }
        
        cell.addValuesToCell(photoUrlForProduct: product.photoForProduct, nameOfProductValue: product.nameOfProduct, currentBidValue: product.maximPaid, endTimeValue: product.endTime, endTimeDay: product.endTimeDay)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.typeOfTable == 1 {
            return self.wonProducts.count
        } else if self.typeOfTable == 2 {
            return self.pendingProducts.count
        }
        return self.addedProducts.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexForCellWithProduct = indexPath.row
        performSegue(withIdentifier: "detailedScreenForProduct", sender: self)
    }
    
    //MARK: Firebase related
    func fetchProductsFromFirebase() {
        refProducts = Database.database().reference().child("products")
        refProducts.observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                self.addedProducts.removeAll()
                self.wonProducts.removeAll()
                self.pendingProducts.removeAll()
                for product in snapshot.children.allObjects as! [DataSnapshot] {
                    
                    let userObject = product.value as? [String: AnyObject]
                    let nameOfProduct = userObject?["nameOfProduct"]
                    let descriptionOfProduct = userObject?["descriptionOfProduct"]
                    let endTime  = userObject?["endTime"]
                    let maximPaid  = userObject?["maximPaid"]
                    let lowestBid  = userObject?["lowestBid"]
                    let photoForProduct = userObject?["photoForProduct"]
                    let endTimeDay = userObject?["endTimeDay"]
                    let userWhoPaidTheMost = userObject?["userWhoPaidTheMost"]
                    let userWhoCreatedAcution = userObject?["userWhoCreatedAcution"]
                    
                    let product = Product(nameOfProduct: (nameOfProduct as! String?)!, descriptionOfProduct: (descriptionOfProduct as! String?)!, endTime: (endTime as! String?)!, maximPaid: (maximPaid as! String?)!, photoForProduct: (photoForProduct as! String?)!, endTimeDay: (endTimeDay as! String?)!, userWhoPaidTheMost: (userWhoPaidTheMost as! String?)!, userWhoCreatedAcution: (userWhoCreatedAcution as! String?)!, lowestBid: (lowestBid as! String?)!)
                    
                    if product.userWhoCreatedAcution == (Auth.auth().currentUser!.uid) {
                        self.addedProducts.append(product)
                    } else if (self.validateTime(endTime: product.endTime) == false || product.endTimeDay != Date().toString(dateFormat: "yyyy:MM:dd")) && product.userWhoPaidTheMost == (Auth.auth().currentUser!.uid) {
                        self.wonProducts.append(product)
                    } else if self.validateTime(endTime: product.endTime) == true && product.userWhoPaidTheMost == (Auth.auth().currentUser!.uid) && product.endTimeDay == Date().toString(dateFormat: "yyyy:MM:dd") {
                        self.pendingProducts.append(product)
                    }
                    
                }
                self.profileTableView.reloadData()
            }
        })
    }
}
