//
//  SettingsViewControllerExtension.swift
//  BidTime
//
//  Created by Raluca Bujor on 24/10/2018.
//  Copyright © 2018 Raluca Bujor. All rights reserved.
//

import Foundation
import UIKit
import Firebase

extension SettingsViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    //MARK: Image Picker Controller delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var selectedImageFromPicker: UIImage?
        
        guard let _ = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            profilePhotoOutlet.setImage(selectedImage, for: .normal)
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("canceled picker")
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Firebase related
    func putInfo() {
        let userID = Auth.auth().currentUser?.uid
        let refUsers = Database.database().reference().child("users").child(userID!)
        refUsers.observe(DataEventType.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                let userObject = snapshot.value as? [String: AnyObject]
                let stringUrl = userObject?["profilePhotoUrl"] as? String ?? ""
                let firstName = userObject?["firstName"] as? String ?? ""
                let lastName = userObject?["lastName"] as? String ?? ""
                
                if let url = URL(string: stringUrl) {
                    if let data = try? Data(contentsOf: url)
                    {
                        self.profilePhotoOutlet.setImage(UIImage(data: data)!, for: .normal)
                        self.firstNameTextField.text = firstName
                        self.lastNameTextField.text = lastName
                    }
                }
            }
        })
    }
    
    func updateValuesFromSaveButton(userID: String, firstName: String, lastName: String, profilePhoto: UIImage) {
        let ref = Database.database().reference();
        
        let storageRef = Storage.storage().reference().child(userID).child("image.png")
        if let uploadData = profilePhoto.pngData() {
            storageRef.putData(uploadData, metadata: nil, completion: {
                (metadata, error) in
                if error != nil {
                    print(error!)
                    return
                }
                print(metadata!)
                storageRef.downloadURL { (URL, error) -> Void in
                    if (error != nil) {
                    } else {
                        let UrlString = URL!.absoluteString
                        ref.child("users").child(userID).child("firstName").setValue(firstName)
                        ref.child("users").child(userID).child("lastName").setValue(lastName)
                        ref.child("users").child(userID).child("profilePhotoUrl").setValue(UrlString)
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.isHidden = true
                    }
                }
            })
        }
    }
    
    func updateNameFromSaveButton(userID: String, firstName: String, lastName: String) {
        let ref = Database.database().reference();
        ref.child("users").child(userID).child("firstName").setValue(firstName)
        ref.child("users").child(userID).child("lastName").setValue(lastName)
    }
    
}
